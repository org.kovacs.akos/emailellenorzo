#include <stdio.h>
#include <stdlib.h>
#include "email.h"
void ellenorzes_futas(const char *email);

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("tul keves vagy tul sok megadott argumentum\n");
        return 1;
    }
    ellenorzes_futas(argv[1]);
    return 0;
}

void ellenorzes_futas(const char *email)
{
    if (email == NULL)
        return;
    if(emailellenorzo(email))
        printf("ervenyes email cim\n");
    else
        printf("ervenytelen email cim\n");
}
