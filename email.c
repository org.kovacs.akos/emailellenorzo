#include <stdio.h>
#include <stdlib.h>
#include "email.h"

int emailellenorzo(const char *email)

{
    int kukacszamlalo = 0;
    int pontszamlalo = 0;
    int karakterszamlalo = 0;



    for (int i = 0; email[i] != '\0'; i++)
    {
        if (email[i] == '@')
        {
            kukacszamlalo++;
            if (email[i + 1] == '@' || email[i + 1] == '.')
                return 0;
        }
        if (email[i] == '.')
        {
            pontszamlalo++;
            if (email[i + 1] == '@' || email[i + 1] == '.')
                return 0;
        }
    }
    if (kukacszamlalo != 1 || pontszamlalo < 1)
        return 0;

    if (email[0] == '@' || email[0] == '.')
        return 0;

    if (email[karakterszamlalo - 1] == '@' || email[karakterszamlalo - 1] == '.')
        return 0;

    return 1;
}
